* Script Installasi SSH KVM CENTOS 6 64BIT
--------
```
wget https://gitlab.com/hugocollier/sshtunnel/-/raw/main/centos/centos6-kvm.sh && chmod +x centos6-kvm.sh && ./centos6-kvm.sh
```

* Script Installasi SSH OPENVZ CENTOS 6 64BIT
--------
```
wget https://gitlab.com/hugocollier/sshtunnel/-/raw/main/centos/centos6-openvz.sh && chmod +x centos6-openvz.sh && ./centos6-openvz.sh
```
* Script Installasi OPENVPN KVM CENTOS 6 64BIT
--------
```
wget https://gitlab.com/hugocollier/sshtunnel/-/raw/main/centos/openvpn/centos.sh && chmod +x centos.sh && ./centos.sh
```
* Script Installasi SSH dan OPENVPN KVM CENTOS 6 64BIT
--------
```
wget https://gitlab.com/hugocollier/sshtunnel/-/raw/main/centos/ssh-vpn.sh && chmod +x ssh-vpn.sh && ./ssh-vpn.sh
```
